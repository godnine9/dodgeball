﻿using System;
using UnityEngine;
using GoogleMobileAds.Api;
using UnityEngine.UI;
public class ADManager : MonoBehaviour
{
    private BannerView bannerView;
    public void Start()
    {
#if UNITY_ANDROID
            string appId = "ca-app-pub-5593845767079260~9787907602";
#elif UNITY_IPHONE
            string appId = "ca-app-pub-5593845767079260~9787907602";
#else
        string appId = "unexpected_platform";
#endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
        RequestBanner();
    }

    public void RequestBanner()
    {
#if UNITY_ANDROID
            string adUnitId = "ca-app-pub-5593845767079260/9376844063";//test
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-5593845767079260/9376844063";
#else
        string adUnitId = "unexpected_platform";
#endif
        this.bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);

        this.bannerView.OnAdLoaded += this.HandleOnAdLoaded;
        this.bannerView.OnAdFailedToLoad += this.HandleOnAdFailedToLoad;
        this.bannerView.OnAdOpening += this.HandleOnAdOpened;
        this.bannerView.OnAdClosed += this.HandleOnAdClosed;
        this.bannerView.OnAdLeavingApplication += this.HandleOnAdLeavingApplication;

        AdRequest request = new AdRequest.Builder().Build();
        this.bannerView.LoadAd(request);
    }
    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        //Debuglog.text = "HandleAdLoaded event received";
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        //Debuglog.text = "HandleFailedToReceiveAd event received with message: " + args.Message;
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        //Debuglog.text = "HandleAdOpened event received";
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        //Debuglog.text = "HandleAdClosed event received";
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        //Debuglog.text = "HandleAdLeavingApplication event received";
    }
}