﻿public static class AudioData
{
    public static readonly string[] BGM = new string[]         { "Audio/BGM/BGM01",
                                                               };
    public static readonly string[] GameSound = new string[]   { "Audio/Sound/Explosion",
                                                                };

    public static readonly string[] UISound = new string[]  {"Audio/Sound/UI/Click",
                                                             "Audio/Sound/UI/Eyecatch01",
                                                             "Audio/Sound/UI/Eyecatch02",
                                                             "Audio/Sound/UI/GameOver",
                                                             "Audio/Sound/UI/HeartReward",
                                                             "Audio/Sound/UI/MoneyReward",
                                                             "Audio/Sound/UI/Reward01",
                                                             "Audio/Sound/UI/Reward02",
                                                             "Audio/Sound/UI/Reward03",
                                                             "Audio/Sound/UI/TouchClick"};
}

public enum BGM
{
    BGM_GAME,
}

public enum GameSound
{
    EXPLOSION,
}