﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager  {

	// (1) 聲音根節點的物體;
    // (2) 保證這個節點在場景切換的時候不會刪除，這樣就不用再初始化一次;
    // (3) 所有播放聲音的生源節點，都是在這個節點下
    static GameObject AudioPlayPool;//這個就是根節點
    static bool IsMusicMute = false;//存放當前全局背景音樂是否靜音的變量
    static bool IsSoundMute = false;//存放當前音效是否靜音的變量

    // url --> AudioSource 映射, 區分音樂，音效
    static Dictionary<string, AudioSource> Music=null;//音樂表
    static Dictionary<string, AudioSource> Sounds = null;//音效表

    static float EffectVolumeAdjust=1f;
    static float MusicVolumeAdjust=1f;


    static bool VolumeSet=false;

    private static string PlayingMusic;

	public static void PowerOnInit()
    {
        AudioPlayPool = new GameObject("AudioPlayPool");//初始化根節點
        AudioPlayPool.AddComponent<AudioScan>();//把聲音檢測組件掛載到根節點下
        GameObject.DontDestroyOnLoad(AudioPlayPool);//場景切換的時候不會刪除根節點
    
    
        //初始化音樂表和音效表
        Music = new Dictionary<string, AudioSource>();
        Sounds = new Dictionary<string, AudioSource>();

        // 從本地來加載這個開關
        if (PlayerPrefs.HasKey("music_mute"))//判斷is_music_mute有沒有保存在本地
        {
            int value = PlayerPrefs.GetInt("music_mute");
            IsMusicMute = (value == 1);//int轉換bool，如果value==1，返回true，否則就是false
        }

        // 從本地來加載這個開關
        if (PlayerPrefs.HasKey("effect_mute"))//判斷is_effect_mute有沒有保存在本地
        {
            int value = PlayerPrefs.GetInt("effect_mute");
            IsSoundMute = (value == 1);//int轉換bool，如果value==1，返回true，否則就是false
        }
    }


    public static AudioSource GetPlayingMusicAudioSource(string url)
    {
        AudioSource audio_source = null;
        if (Music.ContainsKey(url))//判斷是否已經在背景音樂表裏面了
        {
            audio_source = Music[url];//是就直接賦值過去
        }
        return audio_source;
    }

    public static IEnumerator SoundFadeOut (AudioSource audioSource, float FadeTime) {
        float startVolume = audioSource.volume;
 
        while (audioSource.volume > 0) {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
 
            yield return null;
        }
 
        audioSource.Stop ();
        audioSource.volume = startVolume;
    }
    public static IEnumerator MusicFadeOut(float fadeTime)
    {
        AudioSource audioSource = GetPlayingMusicAudioSource(PlayingMusic);
        if (audioSource!=null)
        {
            float startVolume = audioSource.volume;
 
            while (audioSource.volume > 0) {
                audioSource.volume -= startVolume * Time.deltaTime / fadeTime;
                yield return null;
            }
            PlayingMusic = string.Empty;
            audioSource.Stop ();
            audioSource.volume = startVolume;
        }        
    }

    public static IEnumerator MusicFadeIn(string url,float volume, float fadeTime, bool is_loop = true)
    {        
        var audioSource=PlayMusic(url, 0f, is_loop);    
        audioSource.volume = 0f;    
        if (audioSource!=null)
        {           
            while (audioSource.volume < volume) {
                 audioSource.volume += volume * Time.deltaTime / fadeTime;                 
                yield return null;
            }
        }   
        audioSource.volume =volume;     
    }



    //播放指定背景音樂的接口
    public static AudioSource PlayMusic(string url,float volume, bool is_loop = true, float delaytime=0f )
    {
        StopAllMusic();
        AudioSource audio_source = null;
        PlayingMusic = url;
        if (Music.ContainsKey(url))//判斷是否已經在背景音樂表裏面了
        {
            audio_source = Music[url];//是就直接賦值過去
        }
        else//不是就新建一個空節點，節點下再新建一個AudioSource組件
        {
            GameObject s = new GameObject(url);//創建一個空節點
            s.transform.parent = AudioPlayPool.transform;//加入節點到場景中
            
            audio_source = s.AddComponent<AudioSource>();//空節點添加組件AudioSource
            AudioClip clip = Resources.Load<AudioClip>(url);//代碼加載一個AudioClip資源文檔
            audio_source.clip = clip;//設置組件的clip屬性為clip
            //audio_source.loop = is_loop;//設置組件循環播放
            audio_source.playOnAwake = true;//再次喚醒時播放聲音
            audio_source.spatialBlend = 0.0f;//設置為2D聲音
            Music.Add(url, audio_source);//加入到背景音樂字典中，下次就可以直接賦值了
        }

        if (VolumeSet) audio_source.volume=volume*MusicVolumeAdjust;        
        else audio_source.volume=1f;        
        audio_source.loop = is_loop;//設置組件循環播放
        audio_source.mute = IsMusicMute;
        audio_source.enabled = true;
        audio_source.PlayDelayed(delaytime);
        audio_source.Play();//開始播放        

        return audio_source;
    }

    //停止播放指定背景音樂的接口
    public static void StopMusic(string url)
    {
        AudioSource audio_source = null;
        if (!Music.ContainsKey(url))//判斷是否已經在背景音樂表裏面了
        {
            return;//沒有這個背景音樂就直接返回
        }
        PlayingMusic = string.Empty;
        audio_source = Music[url];//有就把audio_source直接賦值過去
        audio_source.Stop();//停止播放
    }

    //停止播放所有背景音樂的接口
    public static void StopAllMusic()
    {
        foreach (AudioSource s in Music.Values)
        {
            PlayingMusic = string.Empty;
            s.Stop();
        }
    }

    //刪除指定背景音樂和它的節點
    public static void ClearMusic(string url)
    {
        AudioSource audio_source = null;
        if (!Music.ContainsKey(url))//判斷是否已經在背景音樂表裏面了
        {
            return;//沒有這個背景音樂就直接返回
        }
        audio_source = Music[url];//有就把audio_source直接賦值過去
        Music[url] = null;//指定audio_source組件清空
        GameObject.Destroy(audio_source.gameObject);//刪除掉掛載指定audio_source組件的節點
    }

    //切換背景音樂靜音開關
    public static void SwitchMusic()
    {
        // 切換靜音和有聲音的狀態
        IsMusicMute = !IsMusicMute;

        //把當前是否靜音寫入本地
        int value = (IsMusicMute) ? 1 : 0;//bool轉換int
        PlayerPrefs.SetInt("music_mute", value);

        // 遍歷所有背景音樂的AudioSource元素
        foreach (AudioSource s in Music.Values)
        {
            s.mute = IsMusicMute;//設置為當前的狀態
        }
    }

    //當我的界面的靜音按鈕要顯示的時候，到底是顯示關閉，還是開始狀態;
    public static bool IsMusicActive()
    {
        return IsMusicMute;
    }


    //接下來開始是音效的接口
    //播放指定音效的接口
    public static void PlaySound(string url,float set_vol, bool is_loop = false, float delaytime=0.0f)
    {
        AudioSource audio_source = null;
        if (Sounds.ContainsKey(url))//判斷是否已經在音效表裏面了
        {
            audio_source = Sounds[url];//是就直接賦值過去           
        }
        else//不是就新建一個空節點，節點下再新建一個AudioSource組件
        {
            GameObject s = new GameObject(url);//創建一個空節點
            s.transform.parent = AudioPlayPool.transform;//加入節點到場景中

            audio_source = s.AddComponent<AudioSource>();//空節點添加組件AudioSource
            AudioClip clip = Resources.Load<AudioClip>(url);//代碼加載一個AudioClip資源文檔
            audio_source.clip = clip;//設置組件的clip屬性為clip
            //audio_source.loop = is_loop;//設置組件循環播放
            audio_source.playOnAwake = true;//再次喚醒時播放聲音
            audio_source.spatialBlend = 0.0f;//設置為2D聲音			
            Sounds.Add(url, audio_source);//加入到音效字典中，下次就可以直接賦值了
        }        
        if (VolumeSet) audio_source.volume = set_vol*EffectVolumeAdjust;
        else audio_source.volume = 1f;
        audio_source.loop = is_loop;//設置組件循環播放
        audio_source.mute = IsSoundMute;
        audio_source.enabled = true;
        audio_source.PlayDelayed(delaytime);
        audio_source.Play();//開始播放
    }


    public static void PlaySoundAdjustVolum(string url,float set_vol, bool is_loop = false, float delaytime=0.0f)
    {
        AudioSource audio_source = null;
        if (Sounds.ContainsKey(url))//判斷是否已經在音效表裏面了
        {
            audio_source = Sounds[url];//是就直接賦值過去           
        }
        else//不是就新建一個空節點，節點下再新建一個AudioSource組件
        {
            GameObject s = new GameObject(url);//創建一個空節點
            s.transform.parent = AudioPlayPool.transform;//加入節點到場景中

            audio_source = s.AddComponent<AudioSource>();//空節點添加組件AudioSource
            AudioClip clip = Resources.Load<AudioClip>(url);//代碼加載一個AudioClip資源文檔
            audio_source.clip = clip;//設置組件的clip屬性為clip            
            audio_source.playOnAwake = true;//再次喚醒時播放聲音
            audio_source.spatialBlend = 0.0f;//設置為2D聲音			
            Sounds.Add(url, audio_source);//加入到音效字典中，下次就可以直接賦值了
        }        
        audio_source.loop = is_loop;//設置組件循環播放
        audio_source.volume = set_vol*EffectVolumeAdjust;
        audio_source.mute = IsSoundMute;
        audio_source.enabled = true;
        audio_source.loop = is_loop;//設置組件循環播放
        audio_source.PlayDelayed(delaytime);
        audio_source.Play();//開始播放
    }

    
    //停止播放指定音效的接口
    public static void StopSound(string url)
    {
        AudioSource audio_source = null;
        if (!Sounds.ContainsKey(url))//判斷是否已經在音效表裏面了
        {
            return;//沒有這個背景音樂就直接返回
        }
        audio_source = Sounds[url];//有就把audio_source直接賦值過去
        audio_source.Stop();//停止播放
    }

    //停止播放所有音效的接口
    public static void StopAllSound()
    {
        foreach (AudioSource s in Sounds.Values)
        {
            s.Stop();
        }
    }

    //刪除指定音效和它的節點
    public static void ClearSound(string url)
    {
        AudioSource audio_source = null;
        if (!Sounds.ContainsKey(url))//判斷是否已經在音效表裏面了
        {
            return;//沒有這個音效就直接返回
        }
        audio_source = Sounds[url];//有就把audio_source直接賦值過去
        Sounds[url] = null;//指定audio_source組件清空
        GameObject.Destroy(audio_source.gameObject);//刪除掉掛載指定audio_source組件的節點
    }

    //切換音效靜音開關
    public static void SwitchSound()
    {
        // 切換靜音和有聲音的狀態
        IsSoundMute = !IsSoundMute;

        //把當前是否靜音寫入本地
        int value = (IsSoundMute) ? 1 : 0;//bool轉換int
        PlayerPrefs.SetInt("effect_mute", value);

        // 遍歷所有音效的AudioSource元素
        foreach (AudioSource s in Sounds.Values)
        {
            s.mute = IsSoundMute;//設置為當前的狀態
        }
    }

    //當我的界面的靜音按鈕要顯示的時候，到底是顯示關閉，還是開始狀態;
    public static bool IsSoundActive()
    {
        return IsSoundMute;
    }

    //播放3D的音效
    public static void Play3DSound(string url, Vector3 pos, bool is_loop = false,  float delaytime=0.0f)
    {
        AudioSource audio_source = null;
        if (Sounds.ContainsKey(url))
        {
            audio_source = Sounds[url];
        }
        else
        {
            GameObject s = new GameObject(url);
            s.transform.parent = AudioPlayPool.transform;
            s.transform.position = pos;//3D音效的位置

            audio_source = s.AddComponent<AudioSource>();
            AudioClip clip = Resources.Load<AudioClip>(url);
            audio_source.clip = clip;
            //audio_source.loop = is_loop;
            audio_source.playOnAwake = true;
            audio_source.spatialBlend = 1.0f; // 3D音效            

            Sounds.Add(url, audio_source);
        }     
        audio_source.loop = is_loop;   
        audio_source.mute = IsSoundMute;
        audio_source.enabled = true;
        audio_source.PlayDelayed(delaytime);
        audio_source.Play();
    }




    //優化策略接口
    public static void disable_over_audio()
    {
        //遍歷背景音樂表
        foreach(AudioSource s in Music.Values){
            if (!s.isPlaying)//判斷是否在播放
            {
                s.enabled = false;//不在播放就直接隱藏
            }
        }

        //遍歷音效表
        foreach (AudioSource s in Sounds.Values)
        {
            if (!s.isPlaying)//判斷是否在播放
            {
                s.enabled = false;//不在播放就直接隱藏
            }
        }
    }

    //調整所有音效音量
    public static void SetAllEffectVolume(float vol)
    {
        EffectVolumeAdjust = vol;
    }

    //調整所有音樂音量
    public static void SetAllMusicVolume(float vol)
    {
        MusicVolumeAdjust = vol;
    }
	
}
