﻿using UnityEngine;

public class AudioScan : MonoBehaviour
{
    void Start()
    {
        this.InvokeRepeating("Scan", 0, 0.5f);
    }

    void Scan()
    {
        AudioManager.disable_over_audio();
    }
}
