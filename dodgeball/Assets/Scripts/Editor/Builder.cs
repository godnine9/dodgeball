﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public static class Builder
{
    [MenuItem("Build/APK")]
    public static void Build()
    {
#if UNITY_ANDROID
        OnAndroid();
#elif UNITY_IOS
        OnIOS();
#endif
    }

    public static void OnAndroid()
    {
        DirectoryInfo dir = Directory.GetParent(Application.dataPath).Parent;
        string path = dir.ToString() + @"\Key.keystore";
        if (File.Exists(path))
        {
            PlayerSettings.Android.keystoreName = path;
            PlayerSettings.Android.keyaliasName = "db";
            PlayerSettings.keystorePass = "RV=aws6A";
            PlayerSettings.keyaliasPass = "3gmn66Pm";
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android,"com.Ninth_Games_Studio.DodgeAsteroids");
            PlayerSettings.companyName = "Ninth_Games_Studio";
            PlayerSettings.productName = "Dodge Asteroids";
            PlayerSettings.SetArchitecture(BuildTargetGroup.Android, 1);
            PlayerSettings.Android.bundleVersionCode = int.Parse(System.DateTime.Now.ToString("yyMMddHHmm"));
        }
        EditorUserBuildSettings.buildAppBundle = false;
        string outPath = Directory.GetParent(Application.dataPath).Parent.ToString() + "/" + PlayerSettings.productName + "_" + System.DateTime.Now.ToString("yyyyMMddHHmm");
        EditorUserBuildSettings.SetBuildLocation(BuildTarget.Android, outPath);

        BuildTarget buildTarget = BuildTarget.Android;
        EditorUserBuildSettings.SwitchActiveBuildTarget( BuildTargetGroup.Android, buildTarget);

        List<string> levels = new List<string>();
        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            if (!scene.enabled) continue;
            levels.Add(scene.path);
        }
        string apkName = string.Format("{0}/{1}.apk", outPath, "Test0228");
        BuildPipeline.BuildPlayer(levels.ToArray(), apkName, buildTarget, BuildOptions.None);
        AssetDatabase.Refresh();
    }

    public static void OnIOS()
    {
        PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.iOS, "com.Ninth_Games_Studio.DodgeAsteroids");
        PlayerSettings.bundleVersion = "1.9.115";
        PlayerSettings.iOS.applicationDisplayName = "Ninth_Games_Studio";
        PlayerSettings.iOS.targetOSVersionString = "8";
        PlayerSettings.iOS.buildNumber = System.DateTime.Now.ToString("yyyyMMddHHmm");
        PlayerSettings.iOS.sdkVersion = iOSSdkVersion.DeviceSDK;
    }
}