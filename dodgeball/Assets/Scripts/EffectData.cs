﻿

public static class EffectData
{
    public static readonly string[] PlayerEffect = new string[]  { "Prefabs/Effect/Dizzy01",
                                                                   "Prefabs/Effect/PowerUp01",
                                                                   "Prefabs/Effect/PowerUp02",
                                                                   "Prefabs/Effect/Punch01",
                                                                   "Prefabs/Effect/Punch02",
                                                                   "Prefabs/Effect/Punch03",
                                                                   "Prefabs/Effect/PunchFly"};
}

public enum PlayerEffect
{
    Dizzy01 = 0,
    PowerUp01,
    PowerUp02,
    Punch01,
    Punch02,
    Punch03,
    PunchFly
}

