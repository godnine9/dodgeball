﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EffectSystem
{
    public static GameObject Get(string EffectPath, Vector3 pos)
    {
        GameObject aGameObject = GamePoolSystem.PoolGet(EffectPath);

        if (aGameObject != null)
        {
            if (!aGameObject.GetComponent<ParticleSpwan>())
            {
                ParticleSpwan p = aGameObject.AddComponent<ParticleSpwan>();
            }

            aGameObject.transform.position = pos;
            aGameObject.transform.rotation = Quaternion.identity;
        }

        return aGameObject;
    }
    public static GameObject HideEffect(string EffectPath)
    {
        GameObject aGameObject = GamePoolSystem.PoolGet(EffectPath);

        if (aGameObject != null)
        {
            if (!aGameObject.GetComponent<ParticleSpwan>())
            {
                ParticleSpwan p = aGameObject.AddComponent<ParticleSpwan>();
            }
            aGameObject.SetActive(false);
        }

        return aGameObject;
    }


    public static GameObject SpawnEffect(string EffectPath, Vector3 pos, float scale)
    {
        GameObject aGameObject = GamePoolSystem.PoolGet(EffectPath);
        if (aGameObject)
        {
            //patricleSystem的 Stop Action要設為 callback,ParticleSpwan才會有用
            if (!aGameObject.GetComponent<ParticleSpwan>())
            {
                aGameObject.AddComponent<ParticleSpwan>();
            }

            aGameObject.transform.position = pos;
            //o.transform.rotation = Quaternion.identity;			
            //if (parent!=null) o.transform.parent = parent;
            ParticleSystem aParticleSystem = aGameObject.GetComponent<ParticleSystem>();
            aGameObject.transform.localScale = new Vector3(scale, scale, scale);
            aParticleSystem.Simulate(0f, true, true);
            aParticleSystem.Play();
        }

        return aGameObject;
    }

    public static GameObject SpawnEffect(string EffectPath, Vector3 pos, Transform parent = null)
    {
        GameObject aGameObject = GamePoolSystem.PoolGet(EffectPath);
        if (aGameObject)
        {
            //patricleSystem的 Stop Action要設為 callback,ParticleSpwan才會有用
            if (!aGameObject.GetComponent<ParticleSpwan>())
            {
                aGameObject.AddComponent<ParticleSpwan>();
            }

            if (parent != null) { aGameObject.transform.SetParent(parent); }
            aGameObject.transform.position = pos;
            aGameObject.transform.rotation = Quaternion.identity;
            ParticleSystem aParticleSystem = aGameObject.GetComponent<ParticleSystem>();
            if (aParticleSystem == null) aParticleSystem = aGameObject.GetComponentInChildren<ParticleSystem>();
            ScaleParticles aScaleParticles = aGameObject.GetComponent<ScaleParticles>();
            aParticleSystem.Simulate(0f, true, true);
            aParticleSystem.Play();
        }

        return aGameObject;
    }

    public static GameObject SpawnEffect(string EffectPath, Vector3 pos)
    {
        GameObject aGameObject = GamePoolSystem.PoolGet(EffectPath);
        if (aGameObject)
        {
            //patricleSystem的 Stop Action要設為 callback,ParticleSpwan才會有用
            if (!aGameObject.GetComponent<ParticleSpwan>())
            {
                aGameObject.AddComponent<ParticleSpwan>();
            }

            aGameObject.transform.position = pos;
            aGameObject.transform.rotation = Quaternion.identity;
            //if (parent!=null) o.transform.parent = parent;
            ParticleSystem aParticleSystem = aGameObject.GetComponent<ParticleSystem>();
            if (aParticleSystem == null) aParticleSystem = aGameObject.GetComponentInChildren<ParticleSystem>();
            aParticleSystem.Simulate(0f, true, true);
            aParticleSystem.Play();
        }

        return aGameObject;
    }

    public static GameObject SpawnEffect(string EffectPath, Vector3 pos, int rx = -90, int rz = 0)
    {
        GameObject aGameObject = GamePoolSystem.PoolGet(EffectPath);

        if (aGameObject)
        {
            //patricleSystem的 Stop Action要設為 callback,ParticleSpwan才會有用
            if (!aGameObject.GetComponent<ParticleSpwan>())
            {
                aGameObject.AddComponent<ParticleSpwan>();
            }

            aGameObject.transform.position = pos;
            aGameObject.transform.rotation = Quaternion.identity;
            aGameObject.transform.Rotate(rx, 0, rz);
            //if (parent!=null) o.transform.parent = parent;
            ParticleSystem aParticleSystem = aGameObject.GetComponent<ParticleSystem>();
            if (aParticleSystem == null) aParticleSystem = aGameObject.GetComponentInChildren<ParticleSystem>();
            aParticleSystem.Simulate(0f, true, true);
            aParticleSystem.Play();
        }

        return aGameObject;
    }

    public static GameObject SpawnEffect(string EffectPath, Vector3 pos, float rx, float ry, float rz)
    {
        GameObject aGameObject = GamePoolSystem.PoolGet(EffectPath);

        if (aGameObject)
        {
            //patricleSystem的 Stop Action要設為 callback,ParticleSpwan才會有用
            if (!aGameObject.GetComponent<ParticleSpwan>())
            {
                aGameObject.AddComponent<ParticleSpwan>();
            }

            aGameObject.transform.position = pos;
            aGameObject.transform.rotation = Quaternion.identity;
            aGameObject.transform.Rotate(rx, ry, rz);
            //if (parent!=null) o.transform.parent = parent;
            ParticleSystem aParticleSystem = aGameObject.GetComponent<ParticleSystem>();
            if (aParticleSystem == null) aParticleSystem = aGameObject.GetComponentInChildren<ParticleSystem>();
            aParticleSystem.Simulate(0f, true, true);
            aParticleSystem.Play();
        }

        return aGameObject;
    }
}
