﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private float Speed;
    private Vector3 MoveDirection;
    private float left, right, down, top;
    public void SpawnEnemy(Vector3 iSpawnPosition, Vector3 iMoveDirection, float iSpeed)
    {
        left = down = -30;
        right = Screen.width + 30;
        top = Screen.height + 30;
        this.transform.position = iSpawnPosition;
        Speed = iSpeed;
        MoveDirection = iMoveDirection;
        float aSize = Screen.height / 90;
        GetComponent<RectTransform>().sizeDelta = new Vector2(aSize , aSize);
        GetComponent<CircleCollider2D>().radius = aSize * 0.5f;
    }

    public void FixedUpdate()
    {
        if (!GameFlowManager.Instance.IsGamePause)
        {
            this.transform.position += MoveDirection * Speed * Time.fixedDeltaTime;
            this.transform.eulerAngles += transform.forward * Speed * Time.fixedDeltaTime;
            if (transform.position.x > right || transform.position.x < left || transform.position.y > top || transform.position.y < down)
            {
                GamePoolSystem.ToPool(this.gameObject);
            }
        }
        if(GameFlowManager.Instance.CurrentStatus != GameStatus.GAME_PLAYING)
        {
            GamePoolSystem.ToPool(this.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            GameObject aVFX = EffectSystem.SpawnEffect("Explostion", this.transform.position);
            AudioManager.PlaySound(AudioData.GameSound[(int)GameSound.EXPLOSION], 1f);
            aVFX.transform.SetParent(this.transform.parent.transform.parent);
            GameFlowManager.Instance.GameResult();
            GamePoolSystem.ToPool(this.gameObject);
        }
    }
}
