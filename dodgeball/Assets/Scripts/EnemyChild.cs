﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChild : MonoBehaviour
{
    public float SizeRiot;
    public void Start()
    {
        float aSize = Screen.height / 90*SizeRiot;
        GetComponent<RectTransform>().sizeDelta = new Vector2(aSize , aSize);
        GetComponent<CircleCollider2D>().radius = aSize * 0.5f * SizeRiot;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            GameObject aVFX = EffectSystem.SpawnEffect("Explostion", this.transform.position);
            AudioManager.PlaySound(AudioData.GameSound[(int)GameSound.EXPLOSION], 1f);
            aVFX.transform.SetParent(this.transform.parent.transform.parent);
            GameFlowManager.Instance.GameResult();
        }
    }
}
