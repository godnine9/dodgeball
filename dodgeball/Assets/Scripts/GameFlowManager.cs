﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameStatus
{
    GAME_LOGO,
    GAME_PLAYING,
    GAME_RESULT,
}

public class GameFlowManager : MonoBehaviour
{
    public static GameFlowManager Instance;
    public GameStatus CurrentStatus;
    public GameObject LogoGroup;
    public GameObject GameGroup;
    public GameObject ResultGroup;
    #region GamePlay
    public GameObject Player;
    public Animator PlayerAim;
    public Text CurrentLevel;
    public Text CurrentTime;
    #endregion
    #region Result
    public Text ResultLevel;
    public Text ResultTime;
    public Text ResultEvaluation;
    #endregion
    [HideInInspector]
    public bool IsGamePause;
    private int mLevel;
    private float ScreenW, ScreenH;
    private float EnemySpwanWidth, EnemySpwanHeight;
    private Vector3 ScreenCenter;
    private float TimeUpdate;
    private float LevelUpdate;
    private float LevelUpTime;
    public void Start()
    {
        Instance = this;
        IsGamePause = false;
        ScreenW = Screen.width;
        ScreenH = Screen.height;
        ScreenCenter = new Vector3(ScreenW * 0.5f, ScreenH * 0.5f, 0);
        EnemySpwanWidth = ScreenW + 30;
        EnemySpwanHeight = ScreenH + 30;
        LevelUpTime = 5.0f;
        AudioManager.PowerOnInit();
        Debug.Log("ScreenW "+ ScreenW+ " ScreenH "+ ScreenH);
        PreloadEnemy();
        SwitchGameStatus(GameStatus.GAME_LOGO);
    }

    void Update()
    {
        switch(CurrentStatus)
        {
            case GameStatus.GAME_PLAYING:
                Gameplay();
                break;
        }
    }

    public void SwitchGameStatus(GameStatus ChangeStatus)
    {
        CurrentStatus = ChangeStatus;
        LogoGroup.SetActive(false);
        GameGroup.SetActive(false);
        ResultGroup.SetActive(false);
        switch (CurrentStatus)
        {
            case GameStatus.GAME_LOGO:
                LogoGroup.SetActive(true);
                break;
            case GameStatus.GAME_PLAYING:
                mLevel = 1;
                TimeUpdate  = 0;
                LevelUpdate = 0;
                LevelUpTime = 5.0f;
                CurrentLevel.text = "01";
                CurrentTime.text = "00.00";
                GamePause(false);
                this.gameObject.GetComponent<VariableJoystick>().ResetInput();
                GameGroup.SetActive(true);
                SpawnEnemy(mLevel);
                AudioManager.PlayMusic(AudioData.BGM[(int)BGM.BGM_GAME], 0.7f, true);
                break;
            case GameStatus.GAME_RESULT:
                ResultLevel.text = CurrentLevel.text;
                ResultTime.text = CurrentTime.text;
                float apoint = TimeUpdate * 100 + mLevel * 1000;
                int aEvalustion = Mathf.FloorToInt(apoint / 3000);
                Debug.Log("aEvalustion  " + aEvalustion +"  "+apoint);
                ResultEvaluation.text = EvaluationName[aEvalustion];
                AudioManager.StopMusic(AudioData.BGM[(int)BGM.BGM_GAME]);
                ResultGroup.SetActive(true);
                break;
        }
    }

    private void PreloadEnemy()
    {
        for (int idx = 0; idx < 100; idx++)
        {
            GameObject aGO = GamePoolSystem.PoolGet("Enemy");
        }
    }

    private void SpawnEnemy(int iLevel)
    {
        int SpawnCount = 50 + iLevel-1;
        for (int idx = 0; idx < SpawnCount; idx++)
        {
            GameObject aGo = GamePoolSystem.PoolGet("Enemy");
            int RandomD = Random.Range(0, 100);
            Vector3 SpawnPosition = Vector3.zero;
            float RandomAmouth = Random.Range(0.0f, 1.1f);
            if (RandomD < 25)
            {
                SpawnPosition = new Vector3(EnemySpwanWidth * RandomAmouth, 0, 0);
            }
            else if(RandomD < 50)
            {
                SpawnPosition = new Vector3(0, EnemySpwanHeight * RandomAmouth, 0);
            }
            else if (RandomD < 75)
            {
                SpawnPosition = new Vector3(EnemySpwanWidth * RandomAmouth, EnemySpwanHeight, 0);
            }
            else if (RandomD < 100)
            {
                SpawnPosition = new Vector3(EnemySpwanWidth, EnemySpwanHeight * RandomAmouth, 0);
            }
            Vector3 MoveDirection = SpawnPosition - ScreenCenter;
            aGo.transform.SetParent(GameGroup.transform);

            float aSpeed = (Screen.height/9) + Random.Range(0, 20) + mLevel;
            aGo.GetComponent<Enemy>().SpawnEnemy(SpawnPosition, MoveDirection.normalized*-1, aSpeed);
        }

        if(iLevel >3)
        {
            int enemy2SpawnCount = Random.Range(0, 10);
            for (int idx = 0; idx < enemy2SpawnCount; idx++)
            {
                GameObject aGo = GamePoolSystem.PoolGet("Enemy2");
                int RandomD = Random.Range(0, 100);
                Vector3 SpawnPosition = Vector3.zero;
                float RandomAmouth = Random.Range(0.0f, 1.1f);
                if (RandomD < 25)
                {
                    SpawnPosition = new Vector3(EnemySpwanWidth * RandomAmouth, 0, 0);
                }
                else if (RandomD < 50)
                {
                    SpawnPosition = new Vector3(0, EnemySpwanHeight * RandomAmouth, 0);
                }
                else if (RandomD < 75)
                {
                    SpawnPosition = new Vector3(EnemySpwanWidth * RandomAmouth, EnemySpwanHeight, 0);
                }
                else if (RandomD < 100)
                {
                    SpawnPosition = new Vector3(EnemySpwanWidth, EnemySpwanHeight * RandomAmouth, 0);
                }
                Vector3 MoveDirection = SpawnPosition - ScreenCenter;
                aGo.transform.SetParent(GameGroup.transform);

                float aSpeed = (Screen.height / 9) + Random.Range(0, 20) + mLevel;
                aGo.GetComponent<Enemy>().SpawnEnemy(SpawnPosition, MoveDirection.normalized * -1, aSpeed);
            }
        }

        if (iLevel > 5)
        {
            int enemy3SpawnCount = Random.Range(0, 5);
            for (int idx = 0; idx < enemy3SpawnCount; idx++)
            {
                GameObject aGo = GamePoolSystem.PoolGet("Enemy3");
                int RandomD = Random.Range(0, 100);
                Vector3 SpawnPosition = Vector3.zero;
                float RandomAmouth = Random.Range(0.0f, 1.1f);
                if (RandomD < 25)
                {
                    SpawnPosition = new Vector3(EnemySpwanWidth * RandomAmouth, 0, 0);
                }
                else if (RandomD < 50)
                {
                    SpawnPosition = new Vector3(0, EnemySpwanHeight * RandomAmouth, 0);
                }
                else if (RandomD < 75)
                {
                    SpawnPosition = new Vector3(EnemySpwanWidth * RandomAmouth, EnemySpwanHeight, 0);
                }
                else if (RandomD < 100)
                {
                    SpawnPosition = new Vector3(EnemySpwanWidth, EnemySpwanHeight * RandomAmouth, 0);
                }
                Vector3 MoveDirection = SpawnPosition - ScreenCenter;
                aGo.transform.SetParent(GameGroup.transform);

                float aSpeed = (Screen.height / 9) + Random.Range(0, 20) + mLevel;
                aGo.GetComponent<Enemy>().SpawnEnemy(SpawnPosition, MoveDirection.normalized * -1, aSpeed);
            }
        }
    }

    public void GameAgain(bool iValue)
    {
        SwitchGameStatus(GameStatus.GAME_PLAYING);
    }

    public void GamePause(bool iValue)
    {
        IsGamePause = iValue;
    }

    private void Gameplay()
    {
        if (!IsGamePause)
        {
            TimeUpdate += Time.deltaTime;
            LevelUpdate += Time.deltaTime;
            if (LevelUpdate > LevelUpTime)
            {
                LevelUpTime -= 0.2f;
                LevelUpdate = 0;
                mLevel += 1;
                if (mLevel < 9)
                {
                    CurrentLevel.text = "0" + mLevel;
                }
                else
                {
                    CurrentLevel.text = mLevel.ToString();
                }
                SpawnEnemy(mLevel);
            }
            CurrentTime.text = TimeUpdate.ToString("0.00");
        }
    }

    public void GameResult()
    {
        IsGamePause = true;
        PlayerAim.Play("Explotion");
        StartCoroutine(GameEnd());
        
    }

    public IEnumerator GameEnd()
    {
        yield return new WaitForSeconds(0.3f);
        CurrentStatus = GameStatus.GAME_RESULT;
        yield return new WaitForSeconds(0.3f);
        this.gameObject.GetComponent<VariableJoystick>().ResetInput();
        SwitchGameStatus(GameStatus.GAME_RESULT);
        PlayerAim.Play("Idle");
        Player.transform.position = ScreenCenter;
    }

    private readonly string[] EvaluationName = new string[]
    {
        "PV1",//二等兵
        "PV2",//一等兵
        "PFC",//上等兵
        "CPL",//下士
        "SGT",//中士
        "SSG",//上士
        "2LT",
        "1LT",
        "CPT",
        "MAJ",
        "LTC",
        "COL",
        "BG",
        "MG",
        "LTG",
        "GEN",
    };
}
