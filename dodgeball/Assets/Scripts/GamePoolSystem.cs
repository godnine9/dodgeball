﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public static class GamePoolSystem
{

	static bool Pause=false;
	static List<SpwanData> SpwanList=new List<SpwanData>();
	//static  GameObject instance=null;
	private  class SpwanData{		
		public   string prefabUrl;
		public 	 GameObject	obj;
		public  List<GameObject> prefabList;
		public void Clear() {
			foreach (GameObject o in prefabList) { 
				o.SetActive(false); 
				Object.Destroy(o);	 
			}
			//Object.DestroyImmediate(obj,true);		
			

			//Object.Destroy(obj);
			//prefabList = null;	

			prefabList.Clear();	
			prefabList=null;
			obj=null;
			prefabUrl=null;
		}		
	}


	public class SpwanFlag : MonoBehaviour {
		public int type_idx=-1;
		public int list_idx=-1;
	}
	
	public static void PowerOnInit() {
		if (SpwanList==null) SpwanList = new List<SpwanData>();
		/*if (instance==null)
		{
	 		instance = new GameObject("gameSpwan");//初始化根節點        
        	GameObject.DontDestroyOnLoad(instance);//場景切換的時候不會刪除根節點
		}*/
	}

	private static bool LoadNewPrefab(string url) {		
		bool s= false;
		var o =Resources.Load<GameObject>(url);
		if (o!=null) {
			SpwanData sp=new SpwanData();			
			sp.prefabList = new List<GameObject>();
			sp.prefabUrl = url;			
			sp.obj = o;
			SpwanList.Add(sp);
			s = true;
		}
		return s;
	}

	public static void ResourceLoadPrefab(string url) {
		PowerOnInit(); 		
		int idx=SpwanList.FindIndex(z => z.prefabUrl == url);

		if (idx==-1) {
			LoadNewPrefab(url);
		}		
	}

	private static GameObject PoolObjectGet(int idx) {
		GameObject g=null;
		if (idx!=-1) {
			int i=SpwanList[idx].prefabList.FindIndex(o => !o.activeInHierarchy);			
			if (i==-1)  { 
				g = (GameObject)Object.Instantiate(SpwanList[idx].obj);
				SpwanList[idx].prefabList.Add(g);
                SpwanFlag sf = g.AddComponent<SpwanFlag>() as SpwanFlag;		
				sf.type_idx = idx;		
				sf.list_idx = SpwanList[idx].prefabList.Count-1;
			}
			else g = SpwanList[idx].prefabList[i];
			if (g) g.SetActive(true);			
		}
		return g;
	}


	private static int PoolObjectActiveNumberGet(int idx) {
		int  number=0;
		if (idx!=-1) {
			//int i=SpwanList[idx].prefabList.FindIndex(o => o.active==false);			
			foreach(GameObject g in SpwanList[idx].prefabList)
			{
				if (g.activeInHierarchy) number++;
			}
		}
		return number;
	}

	public static GameObject PoolGet(string url) {
		if(SpwanList == null) { SpwanList = new List<SpwanData>(); }
		GameObject g=null;
		int idx=SpwanList.FindIndex(z => z.prefabUrl == url);

		if (idx!=-1) {		
			return g=PoolObjectGet(idx);
		}
		else {
			ResourceLoadPrefab(url);
			idx=SpwanList.FindIndex(z => z.prefabUrl == url);
			if (idx!=-1) g=PoolObjectGet(idx);			
		}
		if(g!=null)
		{
			g.transform.parent = null;
		}
		return g;
	}


	public static int PoolGetActiveNumber(string url) {
		
		if (Pause) return 0;
		PowerOnInit(); 
		int number=0;
		int idx=SpwanList.FindIndex(z => z.prefabUrl == url);

		if (idx!=-1) {		
			return number=PoolObjectActiveNumberGet(idx);
		}
		else number=0;

		return number;
	}

	public static void ToPool(GameObject o) {
        if (o == null) { return; }
        SpwanFlag sf = o.GetComponent<SpwanFlag>();
		if (sf != null) o.SetActive(false);
		else GameObject.Destroy(o);		
	}

	public static void Clear(string url) {
		PowerOnInit(); 
		int idx=SpwanList.FindIndex(z => z.prefabUrl == url);
		if (idx!=-1) {
			var sp = SpwanList[idx];
			SpwanList.RemoveAt(idx);
			sp.Clear();	
		}
	}  

	public static void ClearAll() {
		foreach (SpwanData sp in SpwanList) 
		{
			sp.Clear(); 
		}	

		SpwanList.Clear();				
	}

	//public static void LogUsePrefab(string fname)
	public static void LogUsePrefab(string path, string fname, string arrayname)
	{
		//StreamWriter sw; 
 		//FileInfo fi = new FileInfo(fname);
 		//sw = fi.CreateText ();     
		fname=path+fname;   

		FileStream aFile = new FileStream(fname, FileMode.OpenOrCreate);
		StreamWriter sw = new StreamWriter(aFile);

		
		string Dname="static EnemyLoadNum[] "+arrayname+"= {";
		string head="	new EnemyLoadNum( "+'"';
		string end='"'+",";
		string Dend="};";


		sw.WriteLine(Dname);
		foreach (SpwanData sp in SpwanList)
		{			
 			sw.WriteLine(head+ sp.prefabUrl+end+"					"+sp.prefabList.Count + " ),");
		}
		sw.WriteLine(Dend);
 		sw.Close();
 		sw.Dispose();
	}

	public static void PauseSet( bool  pause)
	{
		Pause = pause;
	}
}
