﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpwan : MonoBehaviour
{
    private void DestoryObject() {
        gameObject.transform.parent=null;
        GamePoolSystem.ToPool(gameObject);
	}
 	public void OnParticleSystemStopped()
    {
        DestoryObject();       	
    }    
}
