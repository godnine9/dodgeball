﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public VariableJoystick variableJoystick;
    private RectTransform mPlayer;
    private Vector3 MovePosition;
    private float speed;
    private float right, left, top, down;
    void Start()
    {
        mPlayer = gameObject.GetComponent<RectTransform>();
        MovePosition = Vector3.zero;
        speed = Screen.height / 7;
        right = Screen.width - 25;
        left = 25;
        top = Screen.height - 25;
        down = 25;
    }

    public void FixedUpdate()
    {
        if (!GameFlowManager.Instance.IsGamePause)
        {
            Vector3 direction = Vector3.up * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
            MovePosition = mPlayer.position + direction * speed * Time.fixedDeltaTime;
            if(MovePosition.x > right)
            {
                MovePosition = new Vector3(right, MovePosition.y, 0);
            }
            else if (MovePosition.x < left)
            {
                MovePosition = new Vector3(left, MovePosition.y, 0);
            }
            if (MovePosition.y > top)
            {
                MovePosition = new Vector3(MovePosition.x, top, 0);
            }
            else if (MovePosition.y < down)
            {
                MovePosition = new Vector3(MovePosition.x, down, 0);
            }
            mPlayer.position = MovePosition;
        }
    }
}
