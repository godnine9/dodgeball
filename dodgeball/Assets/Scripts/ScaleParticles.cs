﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ScaleParticles : MonoBehaviour {

// @zpj default scale size;
public float StartScaleSize = 1.0f;
private List<float> initialSizes = new List<float>();
 
	void Awake()
	{
	// Save off all the initial scale values at start.
		ParticleSystem[] particles = gameObject.GetComponentsInChildren<ParticleSystem>();
		foreach (ParticleSystem particle in particles)
		{
			initialSizes.Add(particle.main.startSizeMultiplier);
			ParticleSystemRenderer renderer = particle.GetComponent<ParticleSystemRenderer>();
			if (renderer)
			{
				initialSizes.Add(renderer.lengthScale);
				initialSizes.Add(renderer.velocityScale);
			}
		}

		ScaleSet(StartScaleSize);
	}


	public static void ScaleParticleSystem(GameObject gameObj, float scale)
    {
        var hasParticleObj = false;
        var particles = gameObj.GetComponentsInChildren<ParticleSystem>(true);
        var max = particles.Length;
        for (int idx = 0; idx < max; idx++)
        {
            var particle = particles[idx];
            if(particle==null) continue;
            hasParticleObj = true;
            particle.startSize *= scale;
            particle.startSpeed *= scale;
            particle.startRotation *= scale;
            particle.transform.localScale *= scale;
        }
        if (hasParticleObj)
        {
            gameObj.transform.localScale = new Vector3(scale, scale, scale);
        }
    }

	public void ScaleSet(float scale)
	{
		//ScaleParticleSystem(gameObject, scale);
		//return;

		gameObject.transform.localScale = new Vector3(scale, scale, scale);
		// Scale all the particle components based on parent.
		int arrayIndex = 0;
		ParticleSystem[] particles = gameObject.GetComponentsInChildren<ParticleSystem>();
		foreach (ParticleSystem particle in particles)
		{
			particle.startSize = initialSizes[arrayIndex++] * gameObject.transform.localScale.magnitude;
			ParticleSystemRenderer renderer = particle.GetComponent<ParticleSystemRenderer>();
			if (renderer)
			{
				renderer.lengthScale = initialSizes[arrayIndex++] *
				gameObject.transform.localScale.magnitude;
				renderer.velocityScale = initialSizes[arrayIndex++] *
				gameObject.transform.localScale.magnitude;
			}
		}
	}
}
